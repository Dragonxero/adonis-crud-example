'use strict';

const Car = use('App/Models/Car');

class CarController {
  async index({ view }) {
    const cars = await Car.all();

    return view.render('cars.index', {
      cars: cars.toJSON(),
    });
  }

  async create({ view }) {
    return view.render('cars.new');
  }

  async store({ request, response, view }) {
    const car = new Car();
    car.plate = request.input('plate');
    car.brand = request.input('brand');
    car.model = request.input('model');
    car.year = request.input('year');
    await car.save();

    return response.redirect('/cars');
  }

  async show({ params, view }) {
    const car = await Car.find(params.id);

    return view.render('cars.show', {
      car,
    });
  }

  async edit({ params, view }) {
    const car = await Car.find(params.id);

    return view.render('cars.edit', {
      car,
    });
  }

  async update({ params, request, response }) {
    const car = await Car.find(params.id);
    car.plate = request.input('plate');
    car.brand = request.input('brand');
    car.model = request.input('model');
    car.year = request.input('year');

    await car.save();

    return response.redirect('/cars');
  }

  async destroy({ params, response }) {
    const car = await Car.find(params.id);
    car.delete();

    return response.redirect('/cars');
  }
}

module.exports = CarController;
