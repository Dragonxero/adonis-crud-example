'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class CarSchema extends Schema {
  up() {
    this.create('cars', table => {
      table.increments();
      //Plate / Brand / Model / Year
      table.string('plate', 8).notNullable();
      table.string('brand').notNullable();
      table.string('model').notNullable();
      table.integer('year');
      table.timestamps();
    });
  }

  down() {
    this.drop('cars');
  }
}

module.exports = CarSchema;
